import org.junit.Test;
import roman.RomanConverter;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class GeneralTest {
    @Test
    public void testToRomanWithMax(){
        RomanConverter rc = new RomanConverter();
        int max = 3999;
        assertEquals(rc.toRoman(max),"MMMCMXCIX");
    }
    @Test
    public void testToRomanWithMin(){
        RomanConverter rc = new RomanConverter();
        int min = 1;
        assertEquals(rc.toRoman(min),"I");
    }
    @Test
    public void testFromRomanWithMax(){
        RomanConverter rc = new RomanConverter();
        String max = "MMMCMXCIX";
        assertEquals(rc.fromRoman(max),3999);
    }
    @Test
    public void testFromRomanWithMin(){
        RomanConverter rc = new RomanConverter();
        String min = "I";
        assertEquals(rc.fromRoman(min),1);
    }
    @Test(expected=IllegalArgumentException.class)
    public void testExceptionToRomanHigh(){
        RomanConverter rc = new RomanConverter();
        int over_max = 4000;
        fail(rc.toRoman(over_max));
    }
    @Test(expected=IllegalArgumentException.class)
    public void testExceptionsToRomanLow(){
        RomanConverter rc = new RomanConverter();
        int less_min = 0;
        fail(rc.toRoman(less_min));
    }
    @Test(expected=IllegalArgumentException.class)
    public void testExceptionsFromRoman(){
        RomanConverter rc = new RomanConverter();
        String sample = "ABC";
        rc.fromRoman(sample);
        fail();
    }
    @Test
    public void testAllInRangeToRomanAndBack() {
    		RomanConverter rc = new RomanConverter();
    		String romanStr;
    		int romanInt;
    		for (int i=1; i < 4000; ++i) {
    			romanStr = rc.toRoman(i);
    			romanInt = rc.fromRoman(romanStr);
    			assertEquals(i, romanInt);
    		}
    }
}
